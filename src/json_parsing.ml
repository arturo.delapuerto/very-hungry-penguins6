
let parse_player p =
  let (name,pos) = p in
  match pos with
    |`String s when String.length s = 1 ->
    (name,int_of_char s.[0] - int_of_char 'a')
   |_ -> failwith "The position of a player must be a character."

(*parse players*)
let parse_players p =
  match p with
  | `Assoc l -> List.map parse_player l
  | _ -> failwith ("The player structure should be an Associative list")

let parse_challenge c =
  let (name,n) = c in
  match n with
    |`Int i ->
    begin
      match name with
      |"max fish" -> Move.MAX_FISH i
      |"min fish" -> Move.MIN_FISH i
      |"min teleport" -> Move.MIN_TELEPORT i
      |"min move" -> Move.MIN_MOVE i
      |s -> failwith ("The challenge "^s^" is unknown.")
    end
   |_ -> failwith "The challenge value should be an integer."
		  
let parse_challenges cs =
  match cs with
    | `Assoc l -> List.map parse_challenge l
    | _ -> failwith ("The challenge structure should be an Associative list")
		    
(*parse name players map or turn or challenge*)
let parse_npmt name players_tmp map_file turn challenge_tmp npmtc =
  match fst npmtc with
  |"name" -> begin
      match snd npmtc with
      |`String s -> name := s
      |_ -> failwith "The name of a map should be a string"
    end
  |"map" -> begin
      match snd npmtc with
      |`String s -> map_file := s
      |_ -> failwith "The name of the file containing the map \
                      			     should be a string"
    end
  |"turn" -> begin
      match snd npmtc with
      |`Int i -> turn := i
      |_ -> failwith "The name of the file containing the map \
                      			      should be a string"
    end
  |"players" -> players_tmp := parse_players (snd npmtc)
  |"challenge" -> challenge_tmp := parse_challenges (snd npmtc)
  |_ -> failwith ("The entry "^(fst npmtc)^ " is not understood")


let json_from_file s =
  try
    Yojson.Basic.from_channel (open_in s)
  with
  |Yojson.Json_error log ->
    failwith ("The file is not a json file.\
               	       Here is the log of the json parser : "
              ^log)

let default_player = new Player.humanPlayer "" [|(0,0)|]



(*This is the parser of a json file*)
let parse_main_json json_file =
  let name = ref (String.sub json_file 0 (String.length json_file - 5)) in
  let json_a = json_from_file json_file in
  let players_tmp = ref [] and map_file = ref (!name ^".txt")
  and turn = ref 0  and challenge_tmp = ref [] in
  begin
    match json_a with
    | `Assoc l -> List.iter
		    (parse_npmt name players_tmp map_file turn challenge_tmp) l
    | _ -> failwith "The main structure of should be an Associative list"
  end;

  let map_, peng_pos = IO.parse_map !map_file in

  (*we want to verify that 2 players can not have the same letter*)
  let letter_seen = Array.make (Array.length peng_pos) false in

  (*creation of players*)
  let rec browse_players_tmp l = match l with
    |[] -> []
    |(name,letter)::q ->
      if letter_seen.(letter) then
	failwith "The same letter has been attributed to 2 different players."
      else
	(new Player.humanPlayer name
	     (Array.of_list peng_pos.(letter))) ::
	  browse_players_tmp q
  in

  (!name,browse_players_tmp !players_tmp,map_,!turn, !challenge_tmp)
